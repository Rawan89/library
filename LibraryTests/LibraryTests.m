//
//  LibraryTests.m
//  LibraryTests
//
//  Created by CoDoo Innovative Solutions on 12/29/12.
//  Copyright (c) 2012 CoDoo Innovative Solutions. All rights reserved.
//

#import "LibraryTests.h"

@implementation LibraryTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in LibraryTests");
}

@end
