//
//  Globals.m
//  Library
//
//  Created by Khaled Abul Khair on 1/7/13.
//  Copyright (c) 2013 CoDoo Innovative Solutions. All rights reserved.
//

#import "Globals.h"
static Globals* instance;

@interface Globals ()

@property BOOL loggedIn;
@property (nonatomic,strong) NSString *langCode;
@property (nonatomic,strong) NSBundle *bundle;
@property BOOL isIPhone;

+ (Globals*) getInstance;

@end


@implementation Globals
@synthesize loggedIn=_loggedIn;
@synthesize langCode=_langCode;
@synthesize bundle=_bundle;
@synthesize isIPhone = _isIpad;
- (id)init
{
    self = [super init];
    if (self) {
        self.loggedIn = false;
         NSString *deviceType = [UIDevice currentDevice].model;
        NSLog(@"Device %@",deviceType);
        if([deviceType rangeOfString:@"iPhone" options:NSCaseInsensitiveSearch].location != NSNotFound)
        {
            self.isIPhone = true;
        }else{
            self.isIPhone = false;
        }
    }
    return self;
}

+ (Globals*) getInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[Globals alloc] init];
        NSArray *langs = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
        instance.langCode = [langs objectAtIndex:0];
    });
    return instance;
}

+ (void)userloggedInOut:(BOOL)loggedIn{
    [[Globals getInstance] setLoggedIn:loggedIn];
}

+ (BOOL)isLoggedIn{
    return  [[Globals getInstance] loggedIn];
}


+ (NSString*)userLang{
    return [self getInstance].langCode;
}

+ (void) setUserLang:(NSString*)langCode{
    [[self getInstance] setLangCode:langCode];
}

+ (void)setCurrentBundle:(NSBundle*)bundle{
    [instance setBundle:bundle];
}
+ (NSString*)localizedString:(NSString*)key{
    if(instance.bundle != NULL){
        return [[self getInstance].bundle localizedStringForKey:key value:nil table:@""];
    }else{
        return NSLocalizedString(key, nil);;
    }
}

+ (BOOL)isIphone{
    return [self getInstance].isIPhone;
}

@end
