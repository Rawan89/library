//
//  NonAnimatedPushSegue.m
//  Library
//
//  Created by Khaled Abul Khair on 1/8/13.
//  Copyright (c) 2013 mezo. All rights reserved.
//

#import "NonAnimatedPushSegue.h"

@implementation NonAnimatedPushSegue

- (void) perform {
    
    UIViewController *src = (UIViewController *) self.sourceViewController;
    UIViewController *dst = (UIViewController *) self.destinationViewController;
    
//    [UIView transitionWithView:src.navigationController.view duration:0.2
//                       options:UIViewAnimationOptionTransitionFlipFromLeft
//                    animations:^{
//                        [src.navigationController pushViewController:dst animated:NO];
//                    }
//                    completion:NULL];
    [src.navigationController pushViewController:dst animated:NO];
}


@end
