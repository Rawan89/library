//
//  NonAnimatedPushSegue.h
//  Library
//
//  Created by Khaled Abul Khair on 1/8/13.
//  Copyright (c) 2013 mezo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NonAnimatedPushSegue : UIStoryboardSegue

@end
