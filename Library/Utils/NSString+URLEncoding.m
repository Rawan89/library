//
//  NSString+URLEncoding.m
//  Library
//
//  Created by Khaled Abul Khair on 1/3/13.
//  Copyright (c) 2013 CoDoo Innovative Solutions. All rights reserved.
//

#import "NSString+URLEncoding.h"
#import <Foundation/Foundation.h>

@implementation NSString (URLEncoding)
-(NSString *)urlEncodeUsingEncoding:(NSStringEncoding)encoding {
//	return (__bridge NSString *)CFURLCreateStringByAddingPercentEscapes(NULL,
//                                                               (CFStringRef)self,
//                                                               NULL,
//                                                               (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ABCDEFGHIJKLMNOPQRSTUXYZabcdefghijklmnopqrstuxyz",
//                                                               CFStringConvertNSStringEncodingToEncoding(encoding));
    return (__bridge NSString *)CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                        (CFStringRef)self,
                                                                        NULL,
                                                                        NULL,
                                                                        CFStringConvertNSStringEncodingToEncoding(encoding));
}
@end
