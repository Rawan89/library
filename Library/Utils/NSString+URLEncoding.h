//
//  NSString+URLEncoding.h
//  Library
//
//  Created by Khaled Abul Khair on 1/3/13.
//  Copyright (c) 2013 CoDoo Innovative Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (URLEncoding)
-(NSString *)urlEncodeUsingEncoding:(NSStringEncoding)encoding;

@end
