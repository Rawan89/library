//
//  Subject.m
//  Library
//
//  Created by Khaled Abul Khair on 1/6/13.
//  Copyright (c) 2013 CoDoo Innovative Solutions. All rights reserved.
//

#import "Subject.h"

@implementation Subject

@synthesize name=_name;
@synthesize subjectMain=_subjectMain;
@synthesize subjectId=_authorId;
@synthesize booksCount=_booksCount;

- (id)initWithId:(int)anId name:(NSString*)aName booksCount:(int)aCount andsubMain:(NSString*)aSub
{
    self = [super init];
    if (self) {
        self.subjectId = anId;
        self.name = aName;
        self.booksCount = aCount;
        self.subjectMain = aSub;
        
    }
    return self;
}

- (NSString*) description{
    return [NSString stringWithFormat:@"Subject %d", self.subjectId];
}

@end
