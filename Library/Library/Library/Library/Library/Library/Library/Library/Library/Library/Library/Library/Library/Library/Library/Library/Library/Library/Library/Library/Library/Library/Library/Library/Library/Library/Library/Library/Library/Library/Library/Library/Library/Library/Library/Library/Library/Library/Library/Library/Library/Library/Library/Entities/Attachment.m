//
//  Attachment.m
//  Library
//
//  Created by Khaled Abul Khair on 1/7/13.
//  Copyright (c) 2013 CoDoo Innovative Solutions. All rights reserved.
//

#import "Attachment.h"

@implementation Attachment

@synthesize attId=_attId;
@synthesize fileName=_fileName;
@synthesize fileAccess=_fileAccess;
@synthesize avalibleAra=_avalibleAra;
@synthesize avalibleEng=_avalibleEng;

- (id)initWithId:(int) anId fileName:(NSString*)aFileName fileAccess:(int)aFAccess aAr:(NSString*)avArabic andAEn:(NSString*)avEnglish
{
    self = [super init];
    if (self) {
        self.attId = anId;
        self.fileName = aFileName;
        self.fileAccess = aFAccess;
        self.avalibleAra = avArabic;
        self.avalibleEng = avEnglish;
    }
    return self;
}


@end
