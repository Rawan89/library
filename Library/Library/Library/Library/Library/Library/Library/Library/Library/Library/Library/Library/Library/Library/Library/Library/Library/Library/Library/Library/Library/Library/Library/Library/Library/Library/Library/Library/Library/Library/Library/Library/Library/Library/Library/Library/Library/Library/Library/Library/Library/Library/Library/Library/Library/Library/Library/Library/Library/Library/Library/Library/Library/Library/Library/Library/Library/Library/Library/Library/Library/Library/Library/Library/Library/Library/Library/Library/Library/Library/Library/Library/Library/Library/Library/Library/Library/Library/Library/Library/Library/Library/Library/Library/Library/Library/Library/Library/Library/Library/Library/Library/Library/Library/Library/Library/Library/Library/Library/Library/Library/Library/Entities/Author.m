//
//  Author.m
//  Library
//
//  Created by Khaled Abul Khair on 1/5/13.
//  Copyright (c) 2013 CoDoo Innovative Solutions. All rights reserved.
//

#import "Author.h"

@implementation Author

@synthesize name=_name;
@synthesize subjectMain=_subjectMain;
@synthesize authorId=_authorId;
@synthesize booksCount=_booksCount;

- (id)initWithId:(int)anId name:(NSString*)aName booksCount:(int)aCount andsubMain:(NSString*)aSub
{
    self = [super init];
    if (self) {
        self.authorId = anId;
        self.name = aName;
        self.booksCount = aCount;
        self.subjectMain = aSub;
        
    }
    return self;
}

@end
