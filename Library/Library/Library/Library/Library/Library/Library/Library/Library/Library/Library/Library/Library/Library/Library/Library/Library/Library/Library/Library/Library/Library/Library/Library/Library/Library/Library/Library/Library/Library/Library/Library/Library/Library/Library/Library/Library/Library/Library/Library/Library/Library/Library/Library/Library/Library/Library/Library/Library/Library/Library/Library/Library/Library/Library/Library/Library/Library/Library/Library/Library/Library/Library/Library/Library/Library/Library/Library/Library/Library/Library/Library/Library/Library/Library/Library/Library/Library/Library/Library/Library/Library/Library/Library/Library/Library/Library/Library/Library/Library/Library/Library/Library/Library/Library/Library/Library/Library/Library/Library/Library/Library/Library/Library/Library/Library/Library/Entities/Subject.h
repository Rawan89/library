//
//  Subject.h
//  Library
//
//  Created by Khaled Abul Khair on 1/6/13.
//  Copyright (c) 2013 CoDoo Innovative Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Subject : NSObject

@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *subjectMain;
@property int subjectId;
@property int booksCount;

- (id)initWithId:(int)anId name:(NSString*)aName booksCount:(int)aCount andsubMain:(NSString*)aSub;

@end
