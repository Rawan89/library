//
//  Book.m
//  Library
//
//  Created by Khaled Abul Khair on 1/3/13.
//  Copyright (c) 2013 CoDoo Innovative Solutions. All rights reserved.
//

#import "Book.h"

@implementation Book

@synthesize name=_name;
@synthesize bookId=_bookId;
@synthesize author=_author;
@synthesize publisher=_publisher;

- (id)initWithId:(int)anId andName:(NSString*)aName andAuthor:(NSString*)anAuthor andPubliser:(NSString*)aPublisher
{
    self = [super init];
    if (self) {
        self.bookId = anId;
        self.name = aName;
        self.author = anAuthor;
        self.publisher = aPublisher;
    }
    return self;
}


@end
