//
//  Book.h
//  Library
//
//  Created by Khaled Abul Khair on 1/3/13.
//  Copyright (c) 2013 CoDoo Innovative Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Book : NSObject

@property (nonatomic,strong) NSString* name;
@property int bookId;
@property (nonatomic,strong) NSString* author;
@property (nonatomic,strong) NSString* publisher;

- (id)initWithId:(int)anId andName:(NSString*)aName andAuthor:(NSString*)anAuthor andPubliser:(NSString*)aPublisher;
@end
