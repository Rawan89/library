//
//  Attachment.h
//  Library
//
//  Created by Khaled Abul Khair on 1/7/13.
//  Copyright (c) 2013 CoDoo Innovative Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Attachment : NSObject

@property int attId;
@property (nonatomic,strong) NSString *fileName;
@property  int fileAccess;
@property (nonatomic,strong) NSString *avalibleAra;
@property (nonatomic,strong) NSString *avalibleEng;

- (id)initWithId:(int) anId fileName:(NSString*)aFileName fileAccess:(int)aFAccess aAr:(NSString*)avArabic andAEn:(NSString*)avEnglish;

@end
