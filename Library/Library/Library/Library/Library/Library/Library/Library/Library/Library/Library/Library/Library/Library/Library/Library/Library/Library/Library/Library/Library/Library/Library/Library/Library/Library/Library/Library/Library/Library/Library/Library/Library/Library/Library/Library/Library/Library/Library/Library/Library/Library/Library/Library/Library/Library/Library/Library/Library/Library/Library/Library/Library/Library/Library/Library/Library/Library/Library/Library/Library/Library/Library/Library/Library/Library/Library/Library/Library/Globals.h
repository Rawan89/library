//
//  Globals.h
//  Library
//
//  Created by Khaled Abul Khair on 1/7/13.
//  Copyright (c) 2013 CoDoo Innovative Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Globals : NSObject

+ (void)userloggedInOut:(BOOL)loggedIn;
+ (BOOL)isLoggedIn;
+ (NSString*)userLang;
+ (void)setUserLang:(NSString*)langCode;
+ (void)setCurrentBundle:(NSBundle*)bundle;
+ (NSString*)localizedString:(NSString*)key;
+ (BOOL)isIphone;
@end
