//
//  AppDelegate.h
//  Library
//
//  Created by CoDoo Innovative Solutions on 12/29/12.
//  Copyright (c) 2012 CoDoo Innovative Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (void) reloadStoryBoardWithLang:(NSString*) langCode;

@end
