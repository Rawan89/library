//
//  LoginViewController.h
//  Library
//
//  Created by Khaled Abul Khair on 1/7/13.
//  Copyright (c) 2013 CoDoo Innovative Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController <UITextFieldDelegate>

@end
