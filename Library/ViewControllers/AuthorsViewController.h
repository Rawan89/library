//
//  AuthorsViewController.h
//  Library
//
//  Created by Khaled Abul Khair on 1/5/13.
//  Copyright (c) 2013 CoDoo Innovative Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AuthorsViewController : BaseViewController <UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) NSArray* authers;

@end
