//
//  BaseViewController.m
//  Library
//
//  Created by Khaled Abul Khair on 1/6/13.
//  Copyright (c) 2013 CoDoo Innovative Solutions. All rights reserved.
//

#import "BaseViewController.h"
#import "NavigationController.h"

@interface BaseViewController ()



- (IBAction)goHome:(id)sender;
- (IBAction)goBack:(id)sender;

@end

@implementation BaseViewController
@synthesize loadingAlert = _loadingAlert;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated{
    if(![Globals isIphone])
    {
        [self.navigationController.navigationBar setFrame:CGRectMake(0, 0, self.view.frame.size.width,110.0)];
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.hidesBackButton = YES;
    
    UIView *container = ((NavigationController*)(self.navigationController)).headerButtons;
    // now create a Bar button item
    UIBarButtonItem* item = [[UIBarButtonItem alloc] initWithCustomView:container];
    [item.customView setUserInteractionEnabled:YES];
    
    // set the nav bar's left button item
    self.navigationItem.leftBarButtonItem = item;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)goHome:(id)sender {
    UIViewController* home = [self.navigationController.viewControllers objectAtIndex:1];
    [self.navigationController popToViewController:home animated:NO];
}


@end
