//
//  SearchViewController.h
//  Library
//
//  Created by Khaled Abul Khair on 1/3/13.
//  Copyright (c) 2013 CoDoo Innovative Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface SearchViewController : BaseViewController <UITextFieldDelegate>

@property ( retain, nonatomic) IBOutlet UITextField *searchKeyWord;
@property ( retain, nonatomic) IBOutlet UIButton *titleBtn;
@property (retain, nonatomic) IBOutlet UIButton *authorBtn;
@property (retain, nonatomic) IBOutlet UIButton *subBtn;

- (IBAction)doSearch:(id)sender;

- (IBAction)selectedTitle:(id)sender;
- (IBAction)selectedAuthor:(id)sender;
- (IBAction)selectedSubject:(id)sender;



@end
