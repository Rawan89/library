//
//  BookDetailsViewController.m
//  Library
//
//  Created by Khaled Abul Khair on 1/4/13.
//  Copyright (c) 2013 CoDoo Innovative Solutions. All rights reserved.
//

#import "BookDetailsViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "GDataXMLNode.h"
#import "GTMHTTPFetcher.h"
#import "WebViewController.h"
#import "NSString+URLEncoding.h"
#import "Attachment.h"
#import "AttachmentsViewController.h"

@interface BookDetailsViewController ()
@property ( nonatomic) IBOutlet UILabel *titleLabel;
@property (nonatomic) IBOutlet UILabel *authorLabel;
@property ( nonatomic) IBOutlet UILabel *authorName;
@property ( nonatomic) IBOutlet UILabel *pubDetialsLabel;
@property ( nonatomic) IBOutlet UILabel *publishDetails;

@property (nonatomic,strong) NSString* bookCoverUrl;
@property (nonatomic,strong) NSArray* bookAttachments;

- (IBAction)showBookCover:(id)sender;
- (IBAction)showAttachments:(id)sender;

@end

@implementation BookDetailsViewController

//@synthesize titleLabel=_titleLabel;
//@synthesize authorLabel=_authorLabel;
//@synthesize authorName=_authorName;
//@synthesize pubDetialsLabel=_pubDetialsLabel;
//@synthesize publishDetails=_publishDetails;
//@synthesize currentBook = _currentBook;
//@synthesize bookCoverUrl = _bookCoverUrl;
//@synthesize bookAttachments = _bookAttachments;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIColor * borderColor = [UIColor colorWithRed:0 green:0.31 blue:0.47 alpha:1];
	self.authorLabel.backgroundColor = [UIColor clearColor];
    self.authorLabel.layer.borderColor = borderColor.CGColor;
    self.authorLabel.layer.borderWidth = 2;
    
    self.authorName.backgroundColor = [UIColor clearColor];
    self.authorName.layer.borderColor = borderColor.CGColor;
    self.authorName.layer.borderWidth = 2;

    self.pubDetialsLabel.backgroundColor = [UIColor clearColor];
    self.pubDetialsLabel.layer.borderColor = borderColor.CGColor;
    self.pubDetialsLabel.layer.borderWidth = 2;
    
    self.publishDetails.backgroundColor = [UIColor clearColor];
    self.publishDetails.layer.borderColor = borderColor.CGColor;
    self.publishDetails.layer.borderWidth = 2;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTitleLabel:nil];
    [self setAuthorLabel:nil];
    [self setAuthorName:nil];
    [self setPubDetialsLabel:nil];
    [self setPublishDetails:nil];
    [super viewDidUnload];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.titleLabel.text = self.currentBook.name;
    self.authorName.text = self.currentBook.author;
    self.publishDetails.text = self.currentBook.publisher;
}

- (IBAction)showBookCover:(id)sender {
    NSString* searchUrlString = [NSString stringWithFormat:@"http://libopac.kfsc.edu.sa/webOPAC2_WS/OpacMobileServices.asmx/GetAttachmentCoverURL?LibId=1&Key=%d",self.currentBook.bookId];
    
    //    NSLog(@"Search url:%@",searchUrlString);
    NSURL *url = [NSURL URLWithString:searchUrlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    self.loadingAlert = [[UIAlertView alloc] initWithTitle:[Globals localizedString:@"please_wait"] message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
    [self.loadingAlert show];
    GTMHTTPFetcher* myFetcher = [GTMHTTPFetcher fetcherWithRequest:request];
    [myFetcher beginFetchWithDelegate:self
                    didFinishSelector:@selector(coverFetcher:finishedWithData:error:)];
    
}


- (void)coverFetcher:(GTMHTTPFetcher *)fetcher finishedWithData:(NSData *)retrievedData error:(NSError *)error {
    
    [self.loadingAlert dismissWithClickedButtonIndex:0 animated:YES];
    if (error != nil) {
        // failed; either an NSURLConnection error occurred, or the server returned
        // a status value of at least 300
        //
        // the NSError domain string for server status errors is kGTMHTTPFetcherStatusDomain
        NSLog(@"Error %@",error);
    } else {
        NSLog(@"OK");
        NSString* resp = [[NSString alloc] initWithData:retrievedData encoding:NSUTF8StringEncoding];
        resp = [resp stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
        resp = [resp stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
        GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:resp options:0 error:NULL];
        self.bookCoverUrl = [[doc rootElement] stringValue] ;
        [self performSegueWithIdentifier:@"ShowBookCover" sender:self];
        
    }
}

- (IBAction)showAttachments:(id)sender {
    NSString* searchUrlString = [NSString stringWithFormat:@"http://libopac.kfsc.edu.sa/webOPAC2_WS/OpacMobileServices.asmx/GetBookAttachments?LibId=1&Key=%d",self.currentBook.bookId];
    
    //    NSLog(@"Search url:%@",searchUrlString);
    NSURL *url = [NSURL URLWithString:searchUrlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    self.loadingAlert = [[UIAlertView alloc] initWithTitle:[Globals localizedString:@"please_wait"] message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
    [self.loadingAlert show];
    GTMHTTPFetcher* myFetcher = [GTMHTTPFetcher fetcherWithRequest:request];
    [myFetcher beginFetchWithDelegate:self
                    didFinishSelector:@selector(attachmentsFetcher:finishedWithData:error:)];
}

- (void)attachmentsFetcher:(GTMHTTPFetcher *)fetcher finishedWithData:(NSData *)retrievedData error:(NSError *)error {
    
    [self.loadingAlert dismissWithClickedButtonIndex:0 animated:YES];
    if (error != nil) {
        // failed; either an NSURLConnection error occurred, or the server returned
        // a status value of at least 300
        //
        // the NSError domain string for server status errors is kGTMHTTPFetcherStatusDomain
        NSLog(@"Error %@",error);
    } else {
        NSLog(@"OK");
        NSString* resp = [[NSString alloc] initWithData:retrievedData encoding:NSUTF8StringEncoding];
        resp = [resp stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
        resp = [resp stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
        GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:resp options:0 error:NULL];
        GDataXMLNode *dataset = [[doc rootElement] childAtIndex:0];
        int attNum = (int)[dataset childCount];
        NSLog(@"Attachments count %d",attNum);
        NSMutableArray* attachemnts = [NSMutableArray arrayWithCapacity:attNum];
        
        for(int i=0; i<attNum; i++){
            GDataXMLElement* att_element = (GDataXMLElement*)[dataset childAtIndex:i];
            
            GDataXMLElement *attId_element = [[att_element elementsForName:@"id"] objectAtIndex:0];
            GDataXMLElement *fileName_element = [[att_element elementsForName:@"FileName"] objectAtIndex:0];
            GDataXMLElement *fileAccess_element = [[att_element elementsForName:@"FileAccess"] objectAtIndex:0];
            GDataXMLElement *avalibleAra_element = [[att_element elementsForName:@"Avalible_Ara"] objectAtIndex:0];
            GDataXMLElement *avalibleEng_element = [[att_element elementsForName:@"Avalible_Eng"] objectAtIndex:0];
            
            int attId = [[attId_element stringValue] intValue];
            NSString *fileName = [fileName_element stringValue];
            int fileAccess = [[fileAccess_element stringValue] intValue];
            NSString *avalibleAra = [avalibleAra_element stringValue];
            NSString *avalibleEng = [avalibleEng_element stringValue];
            
            [attachemnts addObject:[[Attachment alloc]initWithId:attId fileName:fileName fileAccess:fileAccess aAr:avalibleAra andAEn:avalibleEng]];
        }
        
        self.bookAttachments = attachemnts;
        [self performSegueWithIdentifier:@"ShowBookAttachments" sender:self];
    
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"ShowBookCover"]){
        WebViewController* dest = segue.destinationViewController;
        dest.url = [self.bookCoverUrl urlEncodeUsingEncoding:NSUTF8StringEncoding];
    }else if([segue.identifier isEqualToString:@"ShowBookAttachments"]){
        AttachmentsViewController* dest = segue.destinationViewController;
        dest.attachments = self.bookAttachments;
    }
}

@end
