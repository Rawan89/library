//
//  BookDetailsViewController.h
//  Library
//
//  Created by Khaled Abul Khair on 1/4/13.
//  Copyright (c) 2013 CoDoo Innovative Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Book.h"

@interface BookDetailsViewController : BaseViewController

@property (nonatomic,strong) Book *currentBook;

@end
