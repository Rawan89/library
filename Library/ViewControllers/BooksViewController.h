//
//  ResultsViewController.h
//  Library
//
//  Created by Khaled Abul Khair on 1/3/13.
//  Copyright (c) 2013 CoDoo Innovative Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "GDataXMLNode.h"

@interface BooksViewController : BaseViewController <UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) NSArray* books;
@end
