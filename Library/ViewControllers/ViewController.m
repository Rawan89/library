//
//  ViewController.m
//  Library
//
//  Created by CoDoo Innovative Solutions on 12/29/12.
//  Copyright (c) 2012 CoDoo Innovative Solutions. All rights reserved.
//

#import "ViewController.h"
#import "Globals.h"

@interface ViewController ()

@end

@implementation ViewController






- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if([Globals isIphone])
    {
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"header_marsad_small.png"] forBarMetrics:UIBarMetricsDefault];
    }
    else {
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"header_marsad_ipad.png"] forBarMetrics:UIBarMetricsDefault];
    }

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
