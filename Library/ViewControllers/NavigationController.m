//
//  NavigationController.m
//  Library
//
//  Created by Khaled Abul Khair on 1/6/13.
//  Copyright (c) 2013 CoDoo Innovative Solutions. All rights reserved.
//

#import "NavigationController.h"
#import "AppDelegate.h"
#import "Globals.h"

@interface NavigationController ()
@property (nonatomic,strong) UIButton* languageBtn;
@property (nonatomic,strong) UIButton* loginOutBtn;

@end

@implementation NavigationController
@synthesize languageBtn = _languageBtn, loginOutBtn=_loginOutBtn;\
@synthesize headerButtons=_headerButtons;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
   
    
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    // create the container
    UIView* container = [[UIView alloc] init];
    //container.backgroundColor = [UIColor redColor];
    CGRect f;
    if([Globals isIphone])
    {
        f = CGRectMake(0, 20, 70, 80);
    } else {
        f = CGRectMake(0,20,180,120);
    }
    container.frame = f;
    
    // create a button and add it to the container
    self.languageBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [self.languageBtn setTitle:[Globals localizedString:@"langBtnLabel"] forState:UIControlStateNormal];
    
    if([Globals isIphone])
    {
        f = CGRectMake(0,5,50,28);
        [self.languageBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:10]];
    } else {
        [self.languageBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:14]];
        f = CGRectMake(0,10,80,40);
    }
    
    self.languageBtn.frame=f;
    [self.languageBtn addTarget:self action:@selector(toggleLanguage:) forControlEvents:UIControlEventTouchUpInside];
    [container addSubview:self.languageBtn];
    
    // add another button
    self.loginOutBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    if([Globals isLoggedIn]){
        [self.loginOutBtn setTitle:[Globals localizedString:@"logoutBtnLabel"] forState:UIControlStateNormal];
    }else{
        [self.loginOutBtn setTitle:[Globals localizedString:@"loginBtnLabel"] forState:UIControlStateNormal];
    }
    if([Globals isIphone])
    {
        f = CGRectMake(0,38,50,28);
        [self.loginOutBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    } else {
        [self.loginOutBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:16]];
        f = CGRectMake(0,60,80,40);
    }
    
    self.loginOutBtn.frame=f;
    [self.loginOutBtn addTarget:self action:@selector(toggleLogin:) forControlEvents:UIControlEventTouchUpInside];
    [container addSubview:self.loginOutBtn];
    self.headerButtons = container;
    
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithCustomView:self.headerButtons];
    self.navigationItem.leftBarButtonItem = anotherButton;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)toggleLogin:(UIButton*)sender{
    if([Globals isLoggedIn]){
        [Globals userloggedInOut:NO];
        [self updateHeader];
    }else{
        UIViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"loginVC"];
        [self presentViewController:loginVC animated:YES completion:nil];
    }
}

- (void)toggleLanguage:(UIButton*)sender{
    NSString *title, *message;
    if ([[Globals userLang] isEqualToString:@"ar"]) {
        title = @"Toggle Language";
        message = @"Application will be restarted to take the effect";
    }else{
        title = @"تغيير اللغة";
        message = @"الرجاء اعد تشغيل التطبيق لتغيير اللغة";
    }
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:title
                                                                  message:message
                                                           preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"OK"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action)
                                {
                                    /** What we write here???????? **/
                                    NSLog(@"you pressed Yes, please button");
                                    if ([[Globals userLang] isEqualToString:@"en"]) {
                                        [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"ar", @"en", nil] forKey:@"AppleLanguages"];
                                        [[NSUserDefaults standardUserDefaults]synchronize];
                                        AppDelegate* app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
                                        [app reloadStoryBoardWithLang:@"ar"];
                                        [Globals setUserLang:@"ar"];
                                    }else{
                                        [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"en", @"ar", nil] forKey:@"AppleLanguages"];
                                        [[NSUserDefaults standardUserDefaults]synchronize];
                                        AppDelegate* app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
                                        [app reloadStoryBoardWithLang:@"en"];
                                        [Globals setUserLang:@"en"];
                                    }
                                    exit(0);
                                    
                                    // call method whatever u need
                                }];
    
    //    UIAlertAction* noButton = [UIAlertAction actionWithTitle:@"No, thanks"
    //                                                       style:UIAlertActionStyleDefault
    //                                                     handler:^(UIAlertAction * action)
    //    {
    //        /** What we write here???????? **/
    //        NSLog(@"you pressed No, thanks button");
    //        // call method whatever u need
    //    }];
    
    [alert addAction:yesButton];
    //    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}
- (void) loginSuccess{
    NSLog(@"login success");
    [Globals userloggedInOut:YES];
    [self updateHeader];
}

- (void) updateHeader{
    NSLog(@"UPdate headers %d",[Globals isLoggedIn]);
    if([Globals isLoggedIn]){
        
        [self.loginOutBtn setTitle:[Globals localizedString:@"logoutBtnLabel"] forState:UIControlStateNormal];
    }else{
        [self.loginOutBtn setTitle:[Globals localizedString:@"loginBtnLabel"] forState:UIControlStateNormal];
    }
    
    
}

@end
