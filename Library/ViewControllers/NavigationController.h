//
//  NavigationController.h
//  Library
//
//  Created by Khaled Abul Khair on 1/6/13.
//  Copyright (c) 2013 CoDoo Innovative Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationController : UINavigationController

@property (nonatomic,strong) UIView* headerButtons;
- (void) loginSuccess;
- (void) updateHeader;

@end
