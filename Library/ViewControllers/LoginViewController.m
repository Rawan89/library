//
//  LoginViewController.m
//  Library
//
//  Created by Khaled Abul Khair on 1/7/13.
//  Copyright (c) 2013 CoDoo Innovative Solutions. All rights reserved.
//

#import "LoginViewController.h"
#import "GDataXMLNode.h"
#import "GTMHTTPFetcher.h"
#import "NavigationController.h"

@interface LoginViewController ()
@property ( nonatomic) IBOutlet UITextField *userNameTF;
@property ( nonatomic) IBOutlet UITextField *passwordTF;
- (IBAction)doLogin:(id)sender;
- (IBAction)hideMe:(id)sender;

@end

@implementation LoginViewController
//@synthesize userNameTF = _userNameTF;
//@synthesize passwordTF = _passwordTF;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setUserNameTF:nil];
    [self setPasswordTF:nil];
    [super viewDidUnload];
}
- (IBAction)doLogin:(id)sender {
    [self.userNameTF resignFirstResponder];
    [self.passwordTF resignFirstResponder];
    NSString* loginUrlString = [NSString stringWithFormat:@"http://libopac.kfsc.edu.sa/webOPAC2_WS/OpacMobileServices.asmx/ValidateUser?LNo=1&UName=%@&PName=%@",self.userNameTF.text,self.passwordTF.text];
    
    //    NSLog(@"Search url:%@",searchUrlString);
    NSURL *url = [NSURL URLWithString:loginUrlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    //[self.loadingAlert show];
    GTMHTTPFetcher* myFetcher = [GTMHTTPFetcher fetcherWithRequest:request];
    [myFetcher beginFetchWithDelegate:self
                    didFinishSelector:@selector(loginFetcher:finishedWithData:error:)];
}

- (void)loginFetcher:(GTMHTTPFetcher *)fetcher finishedWithData:(NSData *)retrievedData error:(NSError *)error {
    
    if (error != nil) {
        // failed; either an NSURLConnection error occurred, or the server returned
        // a status value of at least 300
        //
        // the NSError domain string for server status errors is kGTMHTTPFetcherStatusDomain
        NSLog(@"Error %@",error);
    } else {
        NSLog(@"OK");
        NSString* resp = [[NSString alloc] initWithData:retrievedData encoding:NSUTF8StringEncoding];
        resp = [resp stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
        resp = [resp stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
        GDataXMLDocument * doc = [[GDataXMLDocument alloc] initWithXMLString:resp options:0 error:NULL];
        NSString *retUserName = [[doc rootElement] stringValue];
        if ([retUserName length] >0) {
            NSLog(@"Logged in succes");
            NavigationController* parent = (NavigationController*)[self presentingViewController];
            [parent loginSuccess];
            [self hideMe:nil];
        }else{
            UIAlertView* failAlert = [[UIAlertView alloc] initWithTitle:[Globals localizedString:@"login_failed"] message:[Globals localizedString:@"inv_UserPass"] delegate:nil cancelButtonTitle:[Globals localizedString:@"OK"] otherButtonTitles:nil];
            [failAlert show];
        }
    }
}

- (IBAction)hideMe:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(self.userNameTF.isFirstResponder){
        return [self.passwordTF becomeFirstResponder];
    }else if(self.passwordTF.isFirstResponder){
        [self doLogin:nil];
        return [self.passwordTF resignFirstResponder];
    }
    return false;
    
}

@end
