//
//  AttachmentsViewController.m
//  Library
//
//  Created by Khaled Abul Khair on 1/7/13.
//  Copyright (c) 2013 CoDoo Innovative Solutions. All rights reserved.
//

#import "AttachmentsViewController.h"
#import "Attachment.h"
#import "GDataXMLNode.h"
#import "GTMHTTPFetcher.h"
#import "WebViewController.h"

@interface AttachmentsViewController ()

@property (nonatomic,strong) Attachment* selectedAttachment;
@property (nonatomic,strong) NSString* attachmentUrl;

@end

@implementation AttachmentsViewController

@synthesize attachments=_attachments;
@synthesize selectedAttachment = _selectedAttachment;
@synthesize attachmentUrl=_attachmentUrl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TableView Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.attachments count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell= [tableView dequeueReusableCellWithIdentifier:@"AttachmentCell"];
    
    Attachment* att = [self.attachments objectAtIndex:indexPath.row];
    [cell.textLabel setText:[NSString stringWithFormat:@"%@ (%@)", att.fileName,att.avalibleAra]];
    
    return cell;
}

#pragma mark - TableView Delegate


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    self.selectedAttachment = [self.attachments objectAtIndex:indexPath.row];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(self.selectedAttachment.fileAccess==1 && ![Globals isLoggedIn]){
        UIViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"loginVC"];
        [self presentViewController:loginVC animated:YES completion:nil];
        return;
    }
    //retrieve books
    NSString* searchUrlString = [NSString stringWithFormat:@"http://libopac.kfsc.edu.sa/webOPAC2_WS/OpacMobileServices.asmx/GetAttachmentURL?LibId=1&Key=%d", self.selectedAttachment.attId];
    
    NSURL *url = [NSURL URLWithString:searchUrlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    self.loadingAlert = [[UIAlertView alloc] initWithTitle:[Globals localizedString:@"please_wait"] message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
    [self.loadingAlert show];
    GTMHTTPFetcher* myFetcher = [GTMHTTPFetcher fetcherWithRequest:request];
    [myFetcher beginFetchWithDelegate:self
                    didFinishSelector:@selector(attFetcher:finishedWithData:error:)];
}

- (void)attFetcher:(GTMHTTPFetcher *)fetcher finishedWithData:(NSData *)retrievedData error:(NSError *)error {
    
    [self.loadingAlert dismissWithClickedButtonIndex:0 animated:YES];
    if (error != nil) {
        // failed; either an NSURLConnection error occurred, or the server returned
        // a status value of at least 300
        //
        // the NSError domain string for server status errors is kGTMHTTPFetcherStatusDomain
        NSLog(@"Error %@",error);
    } else {
        NSLog(@"OK");
        NSString* resp = [[NSString alloc] initWithData:retrievedData encoding:NSUTF8StringEncoding];
        resp = [resp stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
        resp = [resp stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
        GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:resp options:0 error:NULL];
        self.attachmentUrl = [[doc rootElement] stringValue] ;
        [self performSegueWithIdentifier:@"ShowAttachment" sender:self];
        
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"ShowAttachment"]){
        WebViewController* dest = segue.destinationViewController;
        NSLog(@"Attt url %@",[self.attachmentUrl urlEncodeUsingEncoding:NSUTF8StringEncoding]);
        dest.url = [self.attachmentUrl urlEncodeUsingEncoding:NSUTF8StringEncoding];
    }
}

@end
