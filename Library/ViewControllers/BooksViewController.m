//
//  ResultsViewController.m
//  Library
//
//  Created by Khaled Abul Khair on 1/3/13.
//  Copyright (c) 2013 CoDoo Innovative Solutions. All rights reserved.
//

#import "BooksViewController.h"
#import "Book.h"
#import "BookDetailsViewController.h"

@interface BooksViewController ()

@property (retain, nonatomic) IBOutlet UITableView *tableVeiw;
@property (nonatomic, strong) Book *selectedBook;


@end

@implementation BooksViewController
//@synthesize books=_books;
//@synthesize tableVeiw=_tableVeiw;
//@synthesize selectedBook=_selectedBook;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTableVeiw:nil];
    [super viewDidUnload];
}

#pragma mark - TableView Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.books count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell= [tableView dequeueReusableCellWithIdentifier:@"ArResultCell"];
    Book* book = [self.books objectAtIndex:indexPath.row];
    
    UILabel * autherLabel = (UILabel *)[cell viewWithTag:100];
    [autherLabel setText:book.author];
    
    
    UILabel * titleLabel = (UILabel *)[cell viewWithTag:101];
    [titleLabel setText:book.name];
    
    //Fix label size
    
    UIFont *font = [UIFont systemFontOfSize:17];
    if(![Globals isIphone]){
        font = [UIFont systemFontOfSize:30];
    }
    //CGSize withinSize = CGSizeMake(tableView.frame.size.width, 1000);
    CGSize size = [book.name sizeWithAttributes:
                   @{NSFontAttributeName: font}];
    //CGSize sizee = [book.name sizeWithFont:font constrainedToSize:withinSize lineBreakMode:NSLineBreakByWordWrapping];
    CGRect f = titleLabel.frame;
    f.size.height = ceilf(size.height);
    titleLabel.frame = f;
    return cell;
}

#pragma mark - TableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    Book* book = [self.books objectAtIndex:indexPath.row];
    NSString *text = book.name;
    
    UIFont *font = [UIFont systemFontOfSize:17];
    int extra = 40;
    if(![Globals isIphone]){
        font = [UIFont systemFontOfSize:30];
        extra = 60;
    }
    //CGSize withinSize = CGSizeMake(tableView.frame.size.width, 1000);
    CGSize size = [text sizeWithAttributes:
                   @{NSFontAttributeName: font}];
    //CGSize size = [text sizeWithFont:font constrainedToSize:withinSize lineBreakMode:UILineBreakModeWordWrap];
    
    return ceilf(size.height) + extra;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    self.selectedBook = [self.books objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"TS-ShowBookDetails" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSLog(@"Prepare for %@",segue.identifier );
    if([segue.identifier isEqualToString:@"TS-ShowBookDetails"]){
        BookDetailsViewController* dest = segue.destinationViewController;
        dest.currentBook = self.selectedBook;
    }
}


@end
