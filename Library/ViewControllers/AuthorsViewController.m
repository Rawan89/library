//
//  AuthorsViewController.m
//  Library
//
//  Created by Khaled Abul Khair on 1/5/13.
//  Copyright (c) 2013 CoDoo Innovative Solutions. All rights reserved.
//

#import "AuthorsViewController.h"
#import "Author.h"
#import "GDataXMLNode.h"
#import "GTMHTTPFetcher.h"
#import "Book.h"
#import "BooksViewController.h"

@interface AuthorsViewController ()

@property (nonatomic,strong) Author *selectedAuther;
@property (nonatomic,strong) NSArray *sAuthorBooks;



@end

@implementation AuthorsViewController

@synthesize authers = _authers;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.authers count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell= [tableView dequeueReusableCellWithIdentifier:@"AutherCell"];
    
    Author* author = [self.authers objectAtIndex:indexPath.row];
    
    [cell.textLabel setText:author.subjectMain];
    return cell;
}

#pragma mark - TableView Delegate


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    self.selectedAuther = [self.authers objectAtIndex:indexPath.row];

    //retrieve books
    NSString* searchUrlString = [NSString stringWithFormat:@"http://libopac.kfsc.edu.sa/webOPAC2_WS/OpacMobileServices.asmx/GetAuthorRecords?LibId=1&Key=%d", self.selectedAuther.authorId];
    
    NSURL *url = [NSURL URLWithString:searchUrlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    self.loadingAlert = [[UIAlertView alloc] initWithTitle:[Globals localizedString:@"please_wait"] message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
    [self.loadingAlert show];
    GTMHTTPFetcher* myFetcher = [GTMHTTPFetcher fetcherWithRequest:request];
    [myFetcher beginFetchWithDelegate:self
                    didFinishSelector:@selector(searchFetcher:finishedWithData:error:)];
}

- (void)searchFetcher:(GTMHTTPFetcher *)fetcher finishedWithData:(NSData *)retrievedData error:(NSError *)error {
    
   [self.loadingAlert dismissWithClickedButtonIndex:0 animated:YES];
    if (error != nil) {
        // failed; either an NSURLConnection error occurred, or the server returned
        // a status value of at least 300
        //
        // the NSError domain string for server status errors is kGTMHTTPFetcherStatusDomain
        NSLog(@"Error %@",error);
    } else {
        NSLog(@"OK");
        NSString* resp = [[NSString alloc] initWithData:retrievedData encoding:NSUTF8StringEncoding];
        resp = [resp stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
        resp = [resp stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
        
        //parse books
        GDataXMLDocument * doc = [[GDataXMLDocument alloc] initWithXMLString:resp options:0 error:NULL];
        GDataXMLNode *dataSet = [[doc rootElement] childAtIndex:0] ;
        int numBooks = (int)[dataSet childCount];
        NSLog(@"found  %d books",numBooks);
        NSMutableArray* books = [NSMutableArray arrayWithCapacity:numBooks];
        //Parse the books
        for (int i=0; i<numBooks; i++) {
            GDataXMLElement *book_element = (GDataXMLElement *)[dataSet childAtIndex:i];
            
            GDataXMLElement *recordId_element = [[book_element elementsForName:@"record_no"] objectAtIndex:0];
            GDataXMLElement *name_element = [[book_element elementsForName:@"Booktitle"] objectAtIndex:0];
            GDataXMLElement *author_element = [[book_element elementsForName:@"author"] objectAtIndex:0];
            GDataXMLElement *pub_element = [[book_element elementsForName:@"pub_name"] objectAtIndex:0];
            
            NSString* name = [name_element stringValue];
            int recordId = [[recordId_element stringValue] intValue];
            NSString* author = [author_element stringValue];
            NSString* publisher = [pub_element stringValue];
            // NSLog(@"id %d title %@",recordId, name);
            
            
            [books addObject:[[Book alloc] initWithId:recordId andName:name andAuthor:author andPubliser:publisher]];
        }
        self.sAuthorBooks= books;
        [self performSegueWithIdentifier:@"AS_ShowAuthorBooks" sender:self];
        
    }
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"AS_ShowAuthorBooks"]){
        BooksViewController* dest =segue.destinationViewController;
        [dest setBooks:self.sAuthorBooks];
    }}



@end
