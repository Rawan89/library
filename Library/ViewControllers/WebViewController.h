//
//  WebViewController.h
//  Library
//
//  Created by Khaled Abul Khair on 1/6/13.
//  Copyright (c) 2013 CoDoo Innovative Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : BaseViewController <UIWebViewDelegate>

@property (nonatomic,strong) NSString *url;

@end
