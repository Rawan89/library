//
//  WebViewController.m
//  Library
//
//  Created by Khaled Abul Khair on 1/6/13.
//  Copyright (c) 2013 CoDoo Innovative Solutions. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()

@property (retain, nonatomic) IBOutlet UIWebView *webView;
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

- (IBAction)hideMe:(id)sender;

@end

@implementation WebViewController
//@synthesize url=_url;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"did load %@", self.url);
    NSURL *url = [NSURL URLWithString:self.url];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:request];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setWebView:nil];
    [self setIndicator:nil];
    [super viewDidUnload];
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    [self.indicator startAnimating];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    NSLog(@"failed to load %@",error);
    [self.indicator stopAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    NSLog(@"loading finish");
    [self.indicator stopAnimating];
}


- (IBAction)hideMe:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
