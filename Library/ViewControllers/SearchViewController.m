//
//  SearchViewController.m
//  Library
//
//  Created by Khaled Abul Khair on 1/3/13.
//  Copyright (c) 2013 CoDoo Innovative Solutions. All rights reserved.
//

#import "SearchViewController.h"
#import "GTMHTTPFetcher.h"
#import "NSString+URLEncoding.h"
#import <Foundation/Foundation.h>
#import "GDataXMLNode.h"
#import "Book.h"
#import "BooksViewController.h"
#import "Author.h"
#import "AuthorsViewController.h"
#import "Subject.h"
#import "SubjectsViewController.h"

typedef enum 
{
    TITLE,
    AUTHOR,
    SUBJECT
} SearchType;

@interface SearchViewController ()


@property(nonatomic,strong) NSArray* ItemsToShow;
@property SearchType searchType;

- (void)handleTitleSearchResult:(NSString*) result;
- (void)handleAuthorSearchResult:(NSString*) result;
- (void)handleSubjectSearchResult:(NSString*) result;

@end

@implementation SearchViewController
//@synthesize searchKeyWord = _searchKeyWord;
//
//@synthesize ItemsToShow = _bookToShow;
//@synthesize titleBtn=_titleBtn,authorBtn=_authorBtn, subBtn = _subBtn;
//@synthesize searchType = _searchType;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
       

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    
    
    [self.titleBtn setSelected:YES];
    self.searchType = TITLE;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setSearchKeyWord:nil];
    [self setSubBtn:nil];
    [self setAuthorBtn:nil];
    [self setTitleBtn:nil];
    [super viewDidUnload];
   
}

- (IBAction)selectedTitle:(id)sender {
    self.titleBtn.selected = YES;
    self.authorBtn.selected = self.subBtn.selected = NO;
    self.searchType = TITLE;
}

- (IBAction)selectedAuthor:(id)sender {
    self.authorBtn.selected = YES;
    self.titleBtn.selected = self.subBtn.selected = NO;
    self.searchType = AUTHOR;
}

- (IBAction)selectedSubject:(id)sender {
    self.subBtn.selected = YES;
    self.authorBtn.selected = self.titleBtn.selected = NO;
    self.searchType = SUBJECT;
}



- (IBAction)doSearch:(id)sender {
    NSString *keyword = [[self.searchKeyWord text] urlEncodeUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"Keyword %@", keyword);
    [self.searchKeyWord resignFirstResponder];
    NSString* serviceMethod = @"SearchByTitle";
    switch (self.searchType) {
        case TITLE:
            serviceMethod = @"SearchByTitle";
            break;
            
        case AUTHOR:
            serviceMethod = @"SearchByAuthor";
            break;
            
        default:
            serviceMethod = @"SearchBySubject";
            break;
    }
    
    NSString* searchUrlString = [NSString stringWithFormat:@"http://libopac.kfsc.edu.sa/webOPAC2_WS/OpacMobileServices.asmx/%@?LibId=1&Content=%@",serviceMethod, keyword];
    
//    NSLog(@"Search url:%@",searchUrlString);
    NSURL *url = [NSURL URLWithString:searchUrlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    self.loadingAlert = [[UIAlertView alloc]initWithTitle:[Globals localizedString:@"searching"] message:NULL delegate:NULL cancelButtonTitle:NULL otherButtonTitles: nil];
    [self.loadingAlert show];
    GTMHTTPFetcher* myFetcher = [GTMHTTPFetcher fetcherWithRequest:request];
    [myFetcher beginFetchWithDelegate:self
                    didFinishSelector:@selector(searchFetcher:finishedWithData:error:)];
}


- (void)searchFetcher:(GTMHTTPFetcher *)fetcher finishedWithData:(NSData *)retrievedData error:(NSError *)error {
    
    [self.loadingAlert dismissWithClickedButtonIndex:0 animated:YES];
    if (error != nil) {
        // failed; either an NSURLConnection error occurred, or the server returned
        // a status value of at least 300
        //
        // the NSError domain string for server status errors is kGTMHTTPFetcherStatusDomain
        NSLog(@"Error %@",error);
    } else {
        NSLog(@"OK");
        NSString* resp = [[NSString alloc] initWithData:retrievedData encoding:NSUTF8StringEncoding];
        resp = [resp stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
        resp = [resp stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
        switch (self.searchType) {
            case TITLE:
                [self handleTitleSearchResult:resp];
                break;
                
            case AUTHOR:
                [self handleAuthorSearchResult:resp];
                break;
                
            case SUBJECT:
                [self handleSubjectSearchResult:resp];
                break;
                
            default:
                break;
        }
        
    }
}


- (void)onKeyboardShow:(NSNotification*)notification{
   CGRect f = self.view.frame;
    if(f.origin.y>=0){
        CGRect keyboardBounds;
        NSDictionary *userInfo = [notification userInfo];
        NSValue *keyboardBoundsValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
        [keyboardBoundsValue getValue:&keyboardBounds];
//        NSLog(@"Keyboard show %@", NSStringFromCGRect(keyboardBounds));
        
        f.origin.y-=keyboardBounds.size.height;
        
        [UIView animateWithDuration:0.5 animations:^{self.view.frame = f;}];
    }
}

- (void)onKeyboardHide:(NSNotification*)notification{
//    NSLog(@"Keyboard hide");
    CGRect f = self.view.frame;
    if(f.origin.y<0){
        CGRect keyboardBounds;
        NSDictionary *userInfo = [notification userInfo];
        NSValue *keyboardBoundsValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
        [keyboardBoundsValue getValue:&keyboardBounds];
        NSLog(@"Keyboard show %@", NSStringFromCGRect(keyboardBounds));
        
        f.origin.y+=keyboardBounds.size.height;
        
        [UIView animateWithDuration:0.5 animations:^{self.view.frame = f;}];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    return [self.searchKeyWord resignFirstResponder];
    
}



- (void)handleTitleSearchResult:(NSString*) result{
    GDataXMLDocument * doc = [[GDataXMLDocument alloc] initWithXMLString:result options:0 error:NULL];
    GDataXMLNode *dataSet = [[doc rootElement] childAtIndex:0] ;
    int numBooks = (int)[dataSet childCount];
    NSLog(@"found  %d books",numBooks);
    NSMutableArray* books = [NSMutableArray arrayWithCapacity:numBooks];
    //Parse the books
    for (int i=0; i<numBooks; i++) {
        GDataXMLElement *book_element = (GDataXMLElement *)[dataSet childAtIndex:i];
        
        GDataXMLElement *recordId_element = [[book_element elementsForName:@"record_no"] objectAtIndex:0];
        GDataXMLElement *name_element = [[book_element elementsForName:@"Booktitle"] objectAtIndex:0];
        GDataXMLElement *author_element = [[book_element elementsForName:@"author"] objectAtIndex:0];
        GDataXMLElement *pub_element = [[book_element elementsForName:@"pub_name"] objectAtIndex:0];
        
        NSString* name = [name_element stringValue];
        int recordId = [[recordId_element stringValue] intValue];
        NSString* author = [author_element stringValue];
        NSString* publisher = [pub_element stringValue];
        // NSLog(@"id %d title %@",recordId, name);
        
        
        [books addObject:[[Book alloc] initWithId:recordId andName:name andAuthor:author andPubliser:publisher]];
    }
    [self setItemsToShow:books];
    [self performSegueWithIdentifier:@"TS-ShowSearchResult" sender:self];
}

- (void)handleAuthorSearchResult:(NSString*) result{
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:result options:0 error:NULL];
    GDataXMLNode *dataSet = [[doc rootElement] childAtIndex:0] ;
    int numAuthors = (int) [dataSet childCount];
    NSLog(@"found  %d authors",numAuthors);
    NSMutableArray* authors = [NSMutableArray arrayWithCapacity:numAuthors];
    for (int i=0; i<numAuthors; i++) {
        GDataXMLElement *author_element = (GDataXMLElement *)[dataSet childAtIndex:i];
        
        GDataXMLElement *id_element = [[author_element elementsForName:@"sno1"] objectAtIndex:0];
        GDataXMLElement *name_element = [[author_element elementsForName:@"name"] objectAtIndex:0];
        GDataXMLElement *subjectMain_element = [[author_element elementsForName:@"SubjectMain"] objectAtIndex:0];
        GDataXMLElement *count_element = [[author_element elementsForName:@"count"] objectAtIndex:0];
        
        NSString* name = [name_element stringValue];
        int authorId = [[id_element stringValue] intValue];
        NSString* subjectMain = [subjectMain_element stringValue];
        int booksCount = [[count_element stringValue] intValue];
        // NSLog(@"id %d title %@",recordId, name);
        
        
        [authors addObject:[[Author alloc] initWithId:authorId name:name booksCount:booksCount andsubMain:subjectMain]];
    }
    [self setItemsToShow:authors];
    [self performSegueWithIdentifier:@"AS-ShowSearchResult" sender:self];
    
}

- (void)handleSubjectSearchResult:(NSString*) result{
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:result options:0 error:NULL];
    GDataXMLNode *dataSet = [[doc rootElement] childAtIndex:0] ;
    int numSubjects = (int) [dataSet childCount];
    NSLog(@"found  %d subjects",numSubjects);
    NSMutableArray* subjects = [NSMutableArray arrayWithCapacity:numSubjects];
    for (int i=0; i<numSubjects; i++) {
        GDataXMLElement *subject_element = (GDataXMLElement *)[dataSet childAtIndex:i];
        
        GDataXMLElement *id_element = [[subject_element elementsForName:@"Sno1"] objectAtIndex:0];
        GDataXMLElement *name_element = [[subject_element elementsForName:@"name"] objectAtIndex:0];
        GDataXMLElement *subjectMain_element = [[subject_element elementsForName:@"SubjectMain"] objectAtIndex:0];
        GDataXMLElement *count_element = [[subject_element elementsForName:@"count"] objectAtIndex:0];
        
        NSString* name = [name_element stringValue];
        int authorId = [[id_element stringValue] intValue];
        NSString* subjectMain = [subjectMain_element stringValue];
        int booksCount = [[count_element stringValue] intValue];
                
        [subjects addObject:[[Subject alloc] initWithId:authorId name:name booksCount:booksCount andsubMain:subjectMain]];
    }
    [self setItemsToShow:subjects];
    [self performSegueWithIdentifier:@"SS-ShowSearchResult" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"TS-ShowSearchResult"]){
        BooksViewController* dest =segue.destinationViewController;
        [dest setBooks:self.ItemsToShow];
    }else if ([segue.identifier isEqualToString:@"AS-ShowSearchResult"]){
        AuthorsViewController* dest = segue.destinationViewController;
        [dest setAuthers:self.ItemsToShow];
    }else if ([segue.identifier isEqualToString:@"SS-ShowSearchResult"]){
        SubjectsViewController* dest = segue.destinationViewController;
        [dest setSubjects:self.ItemsToShow];
    }
}

@end
